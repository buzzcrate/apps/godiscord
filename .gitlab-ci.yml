---
variables:
  IMAGENAME: godiscord
  DEPLOYVER: "latest"

default:
  tags:
    - build

stages:
  - test
  - build_go
  - build_container
  - deploy
  - deploymanifest

.nontest_rules: &nontest_rules
  - if: '$CI_PIPELINE_SOURCE != "push" && $CI_PIPELINE_SOURCE != "merge_request_event"'

.buildahlogin_template: &buildah_login
  before_script: 
    - sudo buildah login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY

.container_artifact_policy:
  artifacts:
    name: $CI_JOB_NAME
    paths:
      - imagehash.txt
    when: always
    expire_in: 5 day

.go_artifact_policy:
  artifacts:
    name: $CI_JOB_NAME
    paths:
      - bot
    when: always
    expire_in: 5 day

.build_bot: &build_bot
  - echo "Building for ${ARCH} in branch ${DEPLOYVER}"
  - go mod tidy
  - CGO_ENABLED=0 go build -o bot main.go 

include:
  - local: '/.gitlab-ci/.ca_cert.yml'

.build_setup:
  script:
    - export imagename=${IMAGENAME}
    - export mycontainer=$(sudo buildah from scratch)
    - mntcont=$(sudo buildah mount $mycontainer)
    # - echo y|sudo pacstrap -i -c $mntcont coreutils busybox ca-certificates
    - echo y|sudo pacstrap -i -c $mntcont busybox
    - sudo mkdir -p $mntcont/usr/bin/
    - sudo mkdir -p $mntcont/usr/sbin/
    - sudo cp bot $mntcont/usr/bin/bot
    - sudo buildah run $mycontainer -- busybox ln -s /usr/bin /bin 
    - sudo buildah run $mycontainer -- busybox ln -s /usr/sbin /sbin 
    - sudo buildah run $mycontainer -- busybox --install -s
    - !reference [.ca_certifications, script] 
    - sudo buildah run $mycontainer -- rm -rf /var/cache/pacman/pkg
    - sudo buildah config --workingdir '/usr/bin' $mycontainer
    # - sudo buildah config --entrypoint '["sleep","15000"]' $mycontainer
    - sudo buildah config --entrypoint '["bot"]' $mycontainer
    - sudo buildah config --arch $ARCH $mycontainer
    - sudo buildah config --label name=$imagename $mycontainer
    - sudo buildah commit $mycontainer $imagename:${DEPLOYVER}-$ARCH
    - sudo buildah rm $mycontainer
    - sudo buildah images|grep $imagename|awk '{print $3}' > imagehash.txt

.commit_push:
  script:
    - export IMAGEHASH=$(cat imagehash.txt)
    - sudo buildah push --format=v2s2 $IMAGEHASH $CI_REGISTRY_IMAGE:${DEPLOYVER}-${ARCH}
    - sudo buildah rmi $IMAGEHASH

test: 
  stage: test
  script:
    - cd corona
    - go mod tidy
    - go test
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"'
    - if: '$CI_PIPELINE_SOURCE == "push"'

build_go_arm:
  stage: build_go
  variables:
    ARCH: arm64
    DEPLOYVER: $CI_COMMIT_BRANCH
  script:
    - *build_bot
  extends:
    - .go_artifact_policy
  tags:
    - build
    - arm64
  rules:
    - *nontest_rules

build_go_amd:
  stage: build_go
  variables:
    ARCH: amd64
    DEPLOYVER: $CI_COMMIT_BRANCH
  script:
    - *build_bot
  extends:
    - .go_artifact_policy
  tags:
    - build
    - amd64
  rules:
    - *nontest_rules
    
build_branch_arm:
  stage: build_container
  extends:
    - .container_artifact_policy
  variables:
    ARCH: arm64
    DEPLOYVER: $CI_COMMIT_BRANCH
  script:
    - !reference [.build_setup, script]
  needs:
    - build_go_arm
  dependencies:
    - build_go_arm
  tags:
    - build
    - arm64
  rules:
    - *nontest_rules
<<: *buildah_login
                     
build_branch_amd:
  stage: build_container
  extends:
    - .container_artifact_policy
  variables:
    ARCH: amd64
    DEPLOYVER: $CI_COMMIT_BRANCH
  script:
    - !reference [.build_setup, script]
  needs:
    - build_go_amd
  dependencies:
    - build_go_amd
  tags:
    - build
    - amd64
  rules:
    - *nontest_rules
<<: *buildah_login

deploy_arm:
  stage: deploy
  needs:
    - build_branch_arm
  dependencies:
    - build_branch_arm
  variables: 
    ARCH: arm64
    DEPLOYVER: $CI_COMMIT_BRANCH
  script:
    - !reference [.commit_push, script]
  tags:
    - build
    - arm64
  rules:
    - *nontest_rules
<<: *buildah_login

deploy_amd:
  stage: deploy
  needs:
    - build_branch_amd
  dependencies:
    - build_branch_amd
  variables: 
    ARCH: amd64
    DEPLOYVER: $CI_COMMIT_BRANCH
  script:
    - !reference [.commit_push, script]
  tags:
    - build
    - amd64
  rules:
    - *nontest_rules
<<: *buildah_login

deploymanifest:
  stage: deploymanifest
  variables:
    DEPLOYVER: $CI_COMMIT_BRANCH
  script:
    - listName=${IMAGENAME}:${DEPLOYVER}
    - sudo buildah manifest create $listName 
    - |
        sudo buildah manifest add --override-arch=arm64 --override-os=linux \
          --os=linux --arch=arm64 --variant v8 $listName docker://$CI_REGISTRY_IMAGE:${DEPLOYVER}-arm64
    - |
        sudo buildah manifest add --override-arch=amd64 --override-os=linux \
          --os=linux --arch=amd64 $listName docker://$CI_REGISTRY_IMAGE:${DEPLOYVER}-amd64
    - sudo buildah manifest push --format=v2s2 $listName docker://$CI_REGISTRY_IMAGE:${DEPLOYVER} 
    - sudo buildah rmi $listName 
  tags:
    - build
  rules:
    - *nontest_rules
<<: *buildah_login

