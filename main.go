// Main function of the go bot.

package main

import (
	"fmt"
	"github.com/bwmarrin/discordgo"
	"local.com/ninja"
	"log"
	"os"
	"os/signal"
	"strings"
)

var (
	// Discord Bot Global Vars
	bot_name      = os.Getenv("BOT_NAME") // Set bot name as environment variable
	dg            *discordgo.Session
	discord_token = os.Getenv("DISCORD_TOKEN") // Discord API Token
	GuildID       = ""
)

func init() {
	var err error
	dg, err = discordgo.New("Bot " + strings.ReplaceAll(discord_token, "\n", ""))

	if err != nil {
		log.Println("Bot could not register with discord", err)
	}
}

// Check to make sure any variable
func VerifyEnv(env_var string) {
	tmpvar := os.Getenv(env_var)
	fmt.Printf("%s = %s\n", env_var, tmpvar)
}

func BotCheckHandler(s *discordgo.Session, r *discordgo.Ready) {
	log.Println("Bot is up!")
}

// This function will be called (due to AddHandler above) every time a new
// message is created on any channel that the authenticated bot has access to.
func messageCreate(s *discordgo.Session, m *discordgo.MessageCreate) {

	// Ignore all messages created by the bot itself
	// This isn't required in this specific example but it's a good practice.
	if m.Author.ID == s.State.User.ID {
		return
	}
	// If the message is "ping" reply with "Pong!"
	if strings.ToLower(m.Content) == "ping" {
		s.ChannelMessageSend(m.ChannelID, "Pong!")
	}

	// If the message is "pong" reply with "Ping!"
	if strings.ToLower(m.Content) == "pong" {
		s.ChannelMessageSend(m.ChannelID, "Ping!")
	}

	// Find callout for bot
	if strings.Contains(m.Content, bot_name) {
		// Now parse looking for content
		command_strings := strings.Split(strings.ToLower(m.Content), " ")

		// Check that the first split is the name of the bot
		if command_strings[0] == bot_name {
			length_commands := len(command_strings)
			if length_commands == 4 {
				switch strings.ToLower(command_strings[1]) {
				case "county":
					s.ChannelMessageSend(m.ChannelID, ninja.CovidCounty(command_strings[2], command_strings[3]))
				}
			} else if length_commands == 3 {
				switch strings.ToLower(command_strings[1]) {
				case "intpm":
					// International stats per million
					s.ChannelMessageSend(m.ChannelID, ninja.CovidInternationalPerMillion(command_strings[2]))
				case "int":
					// International stats
					s.ChannelMessageSend(m.ChannelID, ninja.CovidInternational(command_strings[2]))
				case "state":
					// State Stats
					s.ChannelMessageSend(m.ChannelID, ninja.CovidState(command_strings[2]))
				case "statepm":
					// State Stats
					s.ChannelMessageSend(m.ChannelID, ninja.CovidStatePerMillion(command_strings[2]))
				default:
					// World stats
					s.ChannelMessageSend(m.ChannelID, ninja.CovidInternational(""))
				}
			} else if length_commands == 2 {
				switch strings.ToLower(command_strings[1]) {
				case "intpm":
					// International stats per million
					s.ChannelMessageSend(m.ChannelID, ninja.CovidInternationalPerMillion(""))
				case "int":
					s.ChannelMessageSend(m.ChannelID, ninja.CovidInternational(""))
				default:
					// World stats
					s.ChannelMessageSend(m.ChannelID, ninja.CovidInternational(""))
				}
			}

		}

	}
}

func main() {
	VerifyEnv("DISCORD_TOKEN")
	VerifyEnv("BOT_NAME")

	// Register status message
	dg.AddHandler(BotCheckHandler)

	// Register message handler
	dg.AddHandler(messageCreate)

	// Only set for incoming messages
	dg.Identify.Intents = discordgo.IntentsGuildMessages

	// Open a websocket connect to discord and listen
	err := dg.Open()
	if err != nil {
		log.Println("error opening connection,", err)
	}

	defer dg.Close() // Hold to close program until the discord bot is closed some how.

	// Ctrl-c or other kill handling
	stop := make(chan os.Signal)
	signal.Notify(stop, os.Interrupt)
	<-stop
	log.Println("Gracefully shutting down")
}
