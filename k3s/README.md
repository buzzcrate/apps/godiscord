# Kubernetes files

Made specifically for deployments such as the BuzzCrate project. 

## Use 

After the build process, the deployment should be simply: 
```bash
./secrets.sh <your discord token here>
kubectl apply -f k3s-deployment.yaml
kubectl apply -f secrets.yaml
```

Everything should be working as intended afterwards. 

