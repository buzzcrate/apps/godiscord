// Testing for ninja package
package ninja

import (
	"fmt"
	// "strings"
	"testing"
)

// Test to ensure API can call a company other than USA
func TestGetCountryStats(t *testing.T) {
	got, err := GetCountryStats("Canada")
	if err != nil {
		fmt.Println("Error in function")
		t.Errorf("Failed to connect to API: \n%q", err)
	}

	// fmt.Print(PrettyPrint(got) + "\n")

	gotCountry := got["country"]

	want := "Canada"

	fmt.Printf("Wants: %s Got: %s\n", want, gotCountry)

	if gotCountry != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}

// // Test to ensure world is called as default
// func TestGetCountryStatsDefault(t *testing.T) {
// 	got, err := GetCountryStats("")
// 	if err != nil {
// 		fmt.Println("Error in function")
// 		t.Errorf("Failed to connect to API: \n%q", err)
// 	}
//
// 	fmt.Print(PrettyPrint(got) + "\n")
//
// 	gotCountry := got["country"]
//
// 	want := got["blah"]
//
// 	fmt.Printf("Wants: %s Got: %s # World won't have a country name\n", want, gotCountry)
//
// 	// if it matches, it's a country and not the world.
// 	if gotCountry != want {
// 		t.Errorf("got %q, wanted %q", got, want)
// 	}
// }

// // Test to ensure world is called as default
// func TestGetCountryStatsWorld(t *testing.T) {
// 	got, err := GetCountryStats("WoRlD")
// 	if err != nil {
// 		fmt.Println("Error in function")
// 		t.Errorf("Failed to connect to API: \n%q", err)
// 	}

// 	fmt.Print(PrettyPrint(got) + "\n")

// 	gotCountry := got["country"]

// 	want := got["blah"]

// 	fmt.Printf("Wants: %s Got: %s # World won't have a country name\n", want, gotCountry)

// 	// if it matches, it's a country and not the world.
// 	if gotCountry != want {
// 		t.Errorf("got %q, wanted %q", got, want)
// 	}
// }

// Test reponse message for world
func TestCovidInternational(t *testing.T) {
	got := CovidInternational("")

	fmt.Println("\n#### <Test World Message> ####")
	fmt.Print(got)
	fmt.Println("\n#### </Test World Message> ####")
}

// Test reponse message for Italy per million
func TestCovidInternationalPerMillion(t *testing.T) {
	got := CovidInternationalPerMillion("Italy")

	fmt.Println("\n#### <Test World Message> ####")
	fmt.Print(got)
	fmt.Println("\n#### </Test World Message> ####")
}

// Test to ensure API can call a company other than USA
func TestGetState(t *testing.T) {
	got, err := GetState("PA")
	if err != nil {
		fmt.Println("Error in function")
		t.Errorf("Failed to connect to API: \n%q", err)
	}

	fmt.Println("\n#### <Test State Message> ####")
	fmt.Print(PrettyPrint(got) + "\n")
	fmt.Println("\n#### </Test State Message> ####")

	gotCountry := got["state"]

	want := "Pennsylvania"

	fmt.Printf("Wants: %s Got: %s\n", want, gotCountry)

	if gotCountry != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}

// Test CovidState with New Jersey
func TestCovidState(t *testing.T) {
	got := CovidState("NJ")
	fmt.Println("\n#### <Test State Message> ####")
	fmt.Print(got)
	fmt.Println("\n#### </Test State Message> ####")
}

// Test to ensure API can call a company other than USA
func TestGetCounty(t *testing.T) {
	got, err := GetCounty("MN", "Olmsted")
	if err != nil {
		fmt.Println("Error in function")
		t.Errorf("Failed to connect to API: \n%q", err)
	}

	fmt.Println("\n#### <Test County Message> ####")
	// fmt.Println(PrettyPrintList(got))
	fmt.Println(PrettyPrint(got))
	fmt.Println("\n#### </Test County Message> ####")

	gotCounty := fmt.Sprint(got["province"]) + "," + fmt.Sprint(got["county"])

	want := "Minnesota,Olmsted"

	fmt.Printf("Wants: %s Got: %s\n", want, gotCounty)

	if gotCounty != want {
		t.Errorf("got %q, wanted %q", got, want)
	}
}

func TestCovidCounty(t *testing.T) {
	got := CovidCounty("NJ", "Mercer")
	fmt.Println("\n#### <Test County Message> ####")
	fmt.Print(got)
	fmt.Println("\n#### </Test County Message> ####")
}

func TestBadCovidCounty(t *testing.T) {
	got := CovidCounty("MN", "Mercer")
	fmt.Println("\n#### <Test County Message> ####")
	fmt.Println("State: MN, County: Mercer")
	fmt.Print(got)
	fmt.Println("\n#### </Test County Message> ####")
}
