// Package to connect to https://corona.lmao.ninja/ api's for the corona
// information. This library acts as an interface to parse the output and
// repond to the Discord bot. All functions interest should have a
// corresponding test for functionality.
package ninja

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"strings"
)

const (
	lmaoApiUrl = "https://disease.sh/v3/"
)

// PrettyPrint to print struct in a readable way
func PrettyPrint(i interface{}) string {
	s, _ := json.MarshalIndent(i, "", "\t")
	return string(s)
}

// PrettyPrint to print struct in a readable way
func PrettyPrintList(i []map[string]interface{}) string {
	s := strings.Builder{}

	for _, jsonObj := range i {
		tmp, _ := json.MarshalIndent(jsonObj, "", "\t")

		s.WriteString(string(tmp))
	}
	return s.String()
}

// Returns processed unstructured json of the country in question.
// Take a country name and get marshelled response.
func GetCountryStats(countryName string) (map[string]interface{}, error) {
	var (
		retval map[string]interface{} // return value
		jbuf   bytes.Buffer           // Buffer for json data
	)

	countryString := lmaoApiUrl + "covid-19/all"
	if countryName != "" && strings.ToLower(countryName) != "world" {
		countryString = lmaoApiUrl + "covid-19/countries/" + strings.ToLower(countryName)
	}

	resp, err := http.Get(countryString)
	if err != nil {
		fmt.Println("Failed to get the lmaoApiUrl url: ", countryString)
	} else {
		defer resp.Body.Close()

		_, _ = io.Copy(&jbuf, resp.Body)

		err = json.Unmarshal(jbuf.Bytes(), &retval)
	}

	return retval, err
}

// Creates a string to respond to the query for the bot
func CovidInternational(countryName string) string {
	response := strings.Builder{}

	country, errCountry := GetCountryStats(countryName)
	if errCountry != nil {
		response.WriteString(fmt.Sprintf("Country %s not found. Please try again.", countryName))
		return response.String()
	}

	// Logic will assume world stats are requested
	if countryName == strings.ToLower("world") || countryName == "" {
		response.WriteString("World Statistics\n")
		response.WriteString(fmt.Sprintf("Cases: %.0f ", country["cases"]))
		response.WriteString(fmt.Sprintf("Recovered: %.0f ", country["recovered"]))
		response.WriteString(fmt.Sprintf("Deaths: %.0f \n", country["deaths"]))
		response.WriteString(fmt.Sprintf("TodayCases: %.0f ", country["todayCases"]))
		response.WriteString(fmt.Sprintf("TodayRecovered: %.0f ", country["todayRecovered"]))
		response.WriteString(fmt.Sprintf("TodayDeaths: %.0f ", country["todayDeaths"]))
	} else {
		response.WriteString(fmt.Sprintf("Stats for %s \n", countryName))
		response.WriteString(fmt.Sprintf("Cases: %.0f ", country["cases"]))
		response.WriteString(fmt.Sprintf("Recovered: %.0f ", country["recovered"]))
		response.WriteString(fmt.Sprintf("Deaths: %.0f \n", country["deaths"]))
		response.WriteString(fmt.Sprintf("TodayCases: %.0f ", country["todayCases"]))
		response.WriteString(fmt.Sprintf("TodayRecovered: %.0f ", country["todayRecovered"]))
		response.WriteString(fmt.Sprintf("TodayDeaths: %.0f ", country["todayDeaths"]))
	}

	return response.String()
}

// Creates a string to respond to the query for the bot for countries per million
func CovidInternationalPerMillion(countryName string) string {
	response := strings.Builder{}

	country, errCountry := GetCountryStats(countryName)
	if errCountry != nil {
		response.WriteString(fmt.Sprintf("Country %s not found. Please try again.", countryName))
		return response.String()
	}

	// Logic will assume world stats are requested
	if countryName == strings.ToLower("world") || countryName == "" {
		response.WriteString("World Statistics\n")
		response.WriteString(fmt.Sprintf("CasesPerOneMillion: %.2f ", country["casesPerOneMillion"]))
		response.WriteString(fmt.Sprintf("TestsPerOneMillion: %.2f ", country["testsPerOneMillion"]))
		response.WriteString(fmt.Sprintf("ActivePerOneMillion: %.2f ", country["activePerOneMillion"]))
		response.WriteString(fmt.Sprintf("RecoveredPerOneMillion: %.2f \n", country["recoveredPerOneMillion"]))
		response.WriteString(fmt.Sprintf("DeathsPerOneMillion: %.2f ", country["deathsPerOneMillion"]))
		response.WriteString(fmt.Sprintf("CriticalPerOneMillion: %.2f ", country["criticalPerOneMillion"]))
		response.WriteString(fmt.Sprintf("OneCasePerPeople: %.2f ", country["oneCasePerPeople"]))
		response.WriteString(fmt.Sprintf("OneDeathPerPeople: %.2f ", country["oneDeathPerPeople"]))
	} else {
		response.WriteString(fmt.Sprintf("Stats for %s \n", countryName))
		response.WriteString(fmt.Sprintf("CasesPerOneMillion: %.2f ", country["casesPerOneMillion"]))
		response.WriteString(fmt.Sprintf("TestsPerOneMillion: %.2f ", country["testsPerOneMillion"]))
		response.WriteString(fmt.Sprintf("ActivePerOneMillion: %.2f ", country["activePerOneMillion"]))
		response.WriteString(fmt.Sprintf("RecoveredPerOneMillion: %.2f \n", country["recoveredPerOneMillion"]))
		response.WriteString(fmt.Sprintf("DeathsPerOneMillion: %.2f ", country["deathsPerOneMillion"]))
		response.WriteString(fmt.Sprintf("CriticalPerOneMillion: %.2f ", country["criticalPerOneMillion"]))
		response.WriteString(fmt.Sprintf("OneCasePerPeople: %.2f ", country["oneCasePerPeople"]))
		response.WriteString(fmt.Sprintf("OneDeathPerPeople: %.2f ", country["oneDeathPerPeople"]))
	}

	return response.String()
}

// Get Covid Response for states
func GetState(stateAbrv string) (map[string]interface{}, error) {
	var (
		retval map[string]interface{} // return value
		jbuf   bytes.Buffer           // Buffer for json data
	)

	if len(stateAbrv) != 2 {
		return retval, errors.New("Abreviation is not a 2 letter abreviation of a state.")
	}

	stateString := lmaoApiUrl + "covid-19/countries/USA"
	if stateAbrv != "" && strings.ToLower(stateAbrv) != "world" {
		longName := STATE_ABRV[strings.ToUpper(stateAbrv)]
		fmt.Print(longName)
		fmt.Println("")
		stateString = lmaoApiUrl + "covid-19/states/" + longName
	}

	resp, err := http.Get(stateString)
	if err != nil {
		fmt.Println("Failed to get the lmaoApiUrl url: ", stateString)
	} else {
		defer resp.Body.Close()

		_, _ = io.Copy(&jbuf, resp.Body)

		err = json.Unmarshal(jbuf.Bytes(), &retval)
	}

	return retval, err
}

// Create a string to respond to the state request
func CovidState(stateName string) string {
	response := strings.Builder{}

	stateStats, errCountry := GetState(stateName)
	if errCountry != nil {
		response.WriteString(fmt.Sprintf("State %s not found. Please try again.", stateName))
		return response.String()
	}

	response.WriteString(fmt.Sprintf("Stats for the state of %s\n", stateStats["state"]))
	response.WriteString(fmt.Sprintf("Cases: %.0f ", stateStats["cases"]))
	response.WriteString(fmt.Sprintf("Active: %.0f ", stateStats["active"]))
	response.WriteString(fmt.Sprintf("Recovered: %.0f ", stateStats["recovered"]))
	response.WriteString(fmt.Sprintf("Deaths: %.0f\n", stateStats["deaths"]))
	response.WriteString(fmt.Sprintf("TodayCases: %.0f ", stateStats["todayCases"]))
	response.WriteString(fmt.Sprintf("TodayDeaths: %.0f ", stateStats["todayDeaths"]))
	response.WriteString(fmt.Sprintf("Tests: %.0f ", stateStats["tests"]))

	return response.String()

}

// Create a string to respond to the state request
func CovidStatePerMillion(stateName string) string {
	response := strings.Builder{}

	stateStats, errCountry := GetState(stateName)
	if errCountry != nil {
		response.WriteString(fmt.Sprintf("State %s not found. Please try again.", stateName))
		return response.String()
	}

	response.WriteString(fmt.Sprintf("Stats for the state of %s\n", stateStats["state"]))
	response.WriteString(fmt.Sprintf("CasesPerOneMillion: %.2f ", stateStats["casesPerOneMillion"]))
	response.WriteString(fmt.Sprintf("DeathsPerOneMillion: %.2f\n", stateStats["deathsPerOneMillion"]))
	response.WriteString(fmt.Sprintf("TestsPerOneMillion: %.2f ", stateStats["testsPerOneMillion"]))
	response.WriteString(fmt.Sprintf("Population: %.0f ", stateStats["population"]))

	return response.String()
}

// Get Covid Response for a county in a state
func GetCounty(stateAbrv string, countyName string) (map[string]interface{}, error) {
	var (
		retval map[string]interface{}   // return value
		tmpval []map[string]interface{} // return value
		jbuf   bytes.Buffer             // Buffer for json data
	)

	if len(stateAbrv) != 2 {
		return retval, errors.New("Abreviation is not a 2 letter abreviation of a state.")
	}

	stateString := lmaoApiUrl + "covid-19/jhucsse/counties/" + countyName

	resp, err := http.Get(stateString)
	if err != nil {
		fmt.Println("Failed to get the lmaoApiUrl url: ", stateString)
	} else {
		defer resp.Body.Close()

		_, _ = io.Copy(&jbuf, resp.Body)

		err = json.Unmarshal(jbuf.Bytes(), &tmpval)
	}

	// Last check if good, if nil return error
	if tmpval == nil {
		return retval, errors.New("URL " + stateString + " returned nil. Please double check state and county.")
	}

	if len(stateAbrv) != 2 {
		return retval, errors.New("Abreviation is not a 2 letter abreviation of a state.")
	}

	longName := STATE_ABRV[strings.ToUpper(stateAbrv)]
	fmt.Print(longName)
	fmt.Println("")

	// Probably wrong state
	if longName == STATE_ABRV["blah"] {
		retval = tmpval[0]
	} else {
		for _, state := range tmpval {
			// Find the correct state to return
			if fmt.Sprint(state["province"]) == longName {
				retval = state
			}
		}
	}

	// Last check if good, if nil return error
	if retval == nil {
		err = errors.New("URL " + stateString + " returned nil. Please double check state and county match.")
	}

	return retval, err
}

// Create a string to respond to the county request
func CovidCounty(stateName string, countyName string) string {
	response := strings.Builder{}

	countyStats, errCounty := GetCounty(stateName, countyName)
	if errCounty != nil {
		response.WriteString("Error retrieving state and county: " + errCounty.Error())
		return response.String()
	}

	stats := countyStats["stats"].(map[string]interface{})

	response.WriteString(fmt.Sprintf("Stats for %s in %s updated at %s\n", countyStats["county"], countyStats["province"], countyStats["updatedAt"]))
	response.WriteString(fmt.Sprintf("Confirmed: %.0f ", stats["confirmed"]))
	response.WriteString(fmt.Sprintf("Deaths: %.0f ", stats["deaths"]))
	response.WriteString(fmt.Sprintf("Recovered: %.0f ", stats["recovered"]))

	return response.String()
}
