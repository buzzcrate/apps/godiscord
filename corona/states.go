package ninja

// import (
// 	"encoding/json"
// )

var (
	STATE_ABRV = map[string]string{
		"AL": string("Alabama"),
		"AK": string("Alaska"),
		"AZ": string("Arizona"),
		"AR": string("Arkansas"),
		"CA": string("California"),
		"CO": string("Colorado"),
		"CT": string("Connecticut"),
		"DE": string("Delaware"),
		"FL": string("Florida"),
		"GA": string("Georgia"),
		"HI": string("Hawaii"),
		"ID": string("Idaho"),
		"IL": string("Illinois"),
		"IN": string("Indiana"),
		"IA": string("Iowa"),
		"KS": string("Kansas"),
		"KY": string("Kentucky"),
		"LA": string("Louisiana"),
		"ME": string("Maine"),
		"MD": string("Maryland"),
		"MA": string("Massachusetts"),
		"MI": string("Michigan"),
		"MN": string("Minnesota"),
		"MS": string("Mississippi"),
		"MO": string("Missouri"),
		"MT": string("Montana"),
		"NE": string("Nebraska"),
		"NV": string("Nevada"),
		"NH": string("New Hampshire"),
		"NJ": string("New Jersey"),
		"NM": string("New Mexico"),
		"NY": string("New York"),
		"NC": string("North Carolina"),
		"ND": string("North Dakota"),
		"OH": string("Ohio"),
		"OK": string("Oklahoma"),
		"OR": string("Oregon"),
		"PA": string("Pennsylvania"),
		"RI": string("Rhode Island"),
		"SC": string("South Carolina"),
		"SD": string("South Dakota"),
		"TN": string("Tennessee"),
		"TX": string("Texas"),
		"UT": string("Utah"),
		"VT": string("Vermont"),
		"VA": string("Virginia"),
		"WA": string("Washington"),
		"WV": string("West Virginia"),
		"WI": string("Wisconsin"),
		"WY": string("Wyoming"),
	}
)

type WorldResponse struct {
	updated                int32   `json:"updated"`
	cases                  int32   `json:"cases"`
	todayCases             int32   `json:"todayCases"`
	deaths                 int32   `json:"deaths"`
	todayDeaths            int32   `json:"todayDeaths"`
	recovered              int32   `json:"recovered"`
	todayRecovered         int32   `json:"todayRecovered"`
	active                 int32   `json:"active"`
	critical               int32   `json:"critical"`
	casesPerOneMillion     int32   `json:"casesPerOneMillion"`
	deathsPerOneMillion    int32   `json:"deathsPerOneMillion"`
	tests                  int32   `json:"tests"`
	testsPerOneMillion     int32   `json:"testsPerOneMillion"`
	population             int32   `json:"population"`
	oneCasePerPeople       float64 `json:"oneCasePerPeople"`
	oneDeathPerPeople      float64 `json:"oneDeathPerPeople"`
	oneTestPerPeople       float64 `json:"oneTestPerPeople"`
	activePerOneMillion    float64 `json:"activePerOneMillion"`
	recoveredPerOneMillion float64 `json:"recoveredPerOneMillion"`
	criticalPerOneMillion  float64 `json:"criticalPerOneMillion"`
	affectedCountries      int32   `json:"affectedCountries"`
}

type CountryResponse struct {
	updated                int32   `json:"updated"`
	country                string  `json:"country"`
	cases                  int32   `json:"cases"`
	todayCases             int32   `json:"todayCases"`
	deaths                 int32   `json:"deaths"`
	todayDeaths            int32   `json:"todayDeaths"`
	recovered              int32   `json:"recovered"`
	todayRecovered         int32   `json:"todayRecovered"`
	active                 int32   `json:"active"`
	critical               int32   `json:"critical"`
	casesPerOneMillion     int32   `json:"casesPerOneMillion"`
	deathsPerOneMillion    int32   `json:"deathsPerOneMillion"`
	tests                  int32   `json:"tests"`
	testsPerOneMillion     int32   `json:"testsPerOneMillion"`
	population             int32   `json:"population"`
	continent              string  `json:"continent"`
	oneCasePerPeople       float64 `json:"oneCasePerPeople"`
	oneDeathPerPeople      float64 `json:"oneDeathPerPeople"`
	oneTestPerPeople       float64 `json:"oneTestPerPeople"`
	activePerOneMillion    float64 `json:"activePerOneMillion"`
	recoveredPerOneMillion float64 `json:"recoveredPerOneMillion"`
	criticalPerOneMillion  float64 `json:"criticalPerOneMillion"`
}

type StateResponse struct {
	state               string  `json:"state"`
	updated             int32   `json:"updated"`
	cases               int32   `json:"cases"`
	todayCases          int32   `json:"todayCases"`
	deaths              int32   `json:"deaths"`
	todayDeaths         int32   `json:"todayDeaths"`
	recovered           int32   `json:"recovered"`
	active              int32   `json:"active"`
	casesPerOneMillion  float64 `json:"casesPerOneMillion"`
	deathsPerOneMillion float64 `json:"deathsPerOneMillion"`
	tests               int32   `json:"tests"`
	testsPerOneMillion  float64 `json:"testsPerOneMillion"`
	population          int32   `json:"population"`
}

type CountyResponse struct {
	country     string             `json:"country"`
	province    string             `json:"providence"`
	updatedAt   string             `json:"updatedAt"`
	county      string             `json:"county"`
	stats       CountyReponseStats `json:"stats"`
	coordinates GlobeCoordinates   `json:"coordinates"`
}

type CountyReponseStats struct {
	confirmed int32 `json:"confirmed"`
	deaths    int32 `json:"deaths"`
	recovered int32 `json:"recovered"`
}

type GlobeCoordinates struct {
	latitude  string `json:"latitude"`
	longitude string `json:"longitude"`
}
